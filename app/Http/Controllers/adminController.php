<?php

namespace App\Http\Controllers;
use DB;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class adminController extends Controller
{
    protected $admin="admin@noticeapp.com";
    public function __construct(){
        session_start();
    }
    
    protected function login(Request $request){
        $admin_email=$request->input('admin_email');
        $admin_password=$request->input('admin_password');
        if($admin_email==="admin@noticeapp.com"){
            if(!(empty($admin_email) && empty($admin_password))){
                $admin_email= strtolower($admin_email);
                $query="select remember_token from users where email='$admin_email' limit 1";
                $result=DB::select($query);
                if($result){
                    $admin_token=$result[0]->remember_token;
                    $verify=password_verify($admin_password,$admin_token);
                    if($verify){
                        $_SESSION['name']="admin";
                        $_SESSION['id']=$admin_token;
                        $data = array('success' =>true ,'msg'=>"admin logged in successfully");
                        return json_encode($data);
                    }else{
                        $data = array('success' =>false ,'msg'=>"invalid email id or password");
                        return json_encode($data);    
                    }
                }else{
                    $data = array('success' =>false ,'msg'=>"invalid email id or password");
                    return json_encode($data);    
                }
            }else{
                $data = array('success' =>false ,'msg'=>"please send valid parameters");
                return json_encode($data);
            }    
        }else{
            $data = array('success' =>false ,'msg'=>"invalid email for admin");
            return json_encode($data);
        }
    }
    
    
    
    protected function logout(Request $request){
        session_destroy();
        header("Location: ".url('/'));
        exit();
    }
    
   
    protected function verifyAdmin(){
        if(!(isset($_SESSION['name']) && isset($_SESSION['id']))){
            header("Location: ".url('/'));
            exit();
        }
    }
    
    
    protected function getUsers(Request $request){//function for getting user data in admin panel
        $this->verifyAdmin();
        if(true){
            $query= "select email,name,gender,age, created_at from users where email<>'$this->admin'";
            $result= DB::select($query);
            if($result){
                return json_encode($result);    
            }else{
                return json_encode($result);
            }    
        }else{
            $data = array('msg'=>"Invalid parameter in request");
            return json_encode($data);
        }
        
    }
    
    protected function getNotices(Request $request){//function for getting user data in admin panel
        $this->verifyAdmin();
        if(true){
            $query= "select user_email,user_name,admin_msg, created_at from notices";
            $result= DB::select($query);
            if($result){
                return json_encode($result);    
            }else{
                return json_encode($result);
            }    
        }else{
            $data = array('msg'=>"Invalid parameter in request");
            return json_encode($data);
        }
        
    }
   
    
    protected function sendNotice(Request $request){
        $this->verifyAdmin();
        $name=$request->input('name');
        $email=$request->input('email');
        $msg=$request->input('msg');
        if(!(empty($name) && empty($email) && empty($msg))){
            $query="insert into notices(user_name,user_email,admin_msg) values('$name','$email','$msg')";
            $result=DB::insert($query);
            if($result){
                $data = array('success' =>true ,'msg'=>"message sent successfully");
                return json_encode($data);
            }else{
                $data = array('success' =>false ,'msg'=>"unable to sent message");
                return json_encode($data);
            }    
        }else{
            $data = array('success' =>false ,'msg'=>"please send valid parameters");
            return json_encode($data);
        }    
    }
    
    protected function setting(Request $request){//function for changing the password of admin panel
        $this->verifyAdmin();
        $current_password=$request->input('current_password');
        $new_password=$request->input('new_password');
        if(!(empty($current_password) && empty($new_password))){
            $query="select remember_token from users";
            $result=Db::select($query);
            if($result){
                $verify=password_verify($current_password,$result[0]->remember_token);
                if($verify){
                    $new_token=password_hash($new_password,PASSWORD_DEFAULT);
                    $query="update users set password='$new_password', remember_token='$new_token' where email='$this->admin'";
                    $result=DB::update($query);
                    if($result){
                        $data = array('success' =>true ,'msg'=>"password updated successfully");
                        return json_encode($data);
                    }else{
                        $data = array('success' =>false ,'msg'=>"unable to update password, please try later");
                        return json_encode($data);
                    }
                }else{
                    $data = array('success' =>false ,'msg'=>"invalid current password");
                    return json_encode($data);
                }
            }else{
                $data = array('success' =>false ,'msg'=>"internal server error please try later");
                return json_encode($data);
            }
        }else{
            $data = array('success' =>false ,'msg'=>"please send valid input parameters");
            return json_encode($data);
        }
    }
    
    
    protected function loadTemplate(Request $request){//function for loading the templates 
        $this->verifyAdmin();
        $name=$request->input('template');
        if($name==="settings"){
            return view($name);
        }else if($name==="pricing"){
            $data=$this->getPricing();
            return view($name,$data);
        }else if($name==="about"){
            return view($name);
        }
    }
    
}



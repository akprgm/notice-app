<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::post('/admin/login','adminController@login');//admin panel route for login admin

Route::get('/admin/users','adminController@getUsers');//admin panel get user info route

Route::get('/admin/notices','adminController@getNotices');//getting notice send by admin

Route::get('/admin/sendNotice','adminController@sendNotice');//send notice to particular user

Route::get('/admin/templates','adminController@loadTemplate');//admin panel load Templates

Route::post('/admin/setting','adminController@setting');//route for changing the setting of admin panel

Route::get('/admin/logout','adminController@logout');//admin panel logout 

Route::get('/', function () {//route to login page
    session_start();
    if((isset($_SESSION['name']) && isset($_SESSION['id']))){
        header("Location: ".url('/')."/dashboard");
        exit();
    }else{
         return view('login');   
    }
    
});

Route::get('/dashboard',function(){//route to dashboard page
    session_start();
    if(!(isset($_SESSION['name']) && isset($_SESSION['id']))){
        header("Location: ".url('/'));
        exit();
    }else{
         return view('dashboard');   
    }
        
});


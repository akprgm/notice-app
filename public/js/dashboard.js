(function dashboard(){
    
    
    function changeActiveMenuItem(ref){//function for changing the menu hover 
        $('#menu>div>ul>li .active').removeClass('active');
        ref.addClass('active');
    }
    
    function templateLoader(template,resolve){//function for loading different templates
        var param="template="+template;
        $.ajax({
            method:'get',
            url:'/admin/templates',
            dataType:'html',
            data:param,
            success:function(res){
                $('#right').html(res);
                resolve('success');         
            },
            error:function(){
                console.log("failure");
            } 
        });        
    }
    
    
    //binding events
    $('#setting').on('click',function(){//changing admin panel password
        changeActiveMenuItem($('#setting'));
        var loadTemplatePromise=new Promise(function(resolve,reject){
            templateLoader("settings",resolve);
        });
        loadTemplatePromise.then(
            function(){
                $('#changePassword').on('submit',function(e){
                    e.preventDefault();
                    var currentPassword=$('#currentPassword').val();
                    var newPassword=$('#newPassword').val();
                    var confirmPassword=$('#confirmPassword').val()
                    if(newPassword===confirmPassword){
                        var obj ={
                            current_password:currentPassword,
                            new_password:newPassword
                        }
                        
                        $.ajax({
                            method:'post',
                            url:'/admin/setting',
                            dataType:'json',
                            data:obj,
                            success:function(response){
                                if(response.success){
                                    $('#errorMsg>span').html(response.msg);
                                    $('#errorMsg').css("color","green");
                                    $('#errorMsg').removeClass('fa-times');
                                    $('#errorMsg').addClass('fa-check');
                                    $('#errorMsg').removeClass('hide');
                                    document.getElementById('changePassword').reset();
                                    setTimeout(function(){
                                        $('#errorMsg').hide(function(){
                                            $('#errorMsg').removeClass('fa-check');
                                            $('#errorMsg').addClass('fa-times');
                                            $('#errorMsg').addClass('hide');
                                            $('#errorMsg').css("display","block");
                                            $('#errorMsg').css("color","red");
                                        });    
                                    },3000);
                                }else{
                                    $('#errorMsg>span').html(response.msg);
                                    $('#errorMsg').removeClass('hide');
                                    document.getElementById('changePassword').reset();
                                    setTimeout(function(){
                                        $('#errorMsg').hide(function(){
                                            $('#errorMsg').addClass('hide');
                                            $('#errorMsg').css("display","block");
                                        });    
                                    },3000);
                                }               
                            },
                            error:function(){
                                    $('#errorMsg>span').html("Please check your network connection");
                                    $('#errorMsg').removeClass('hide');
                                    document.getElementById('login').reset();
                                    setTimeout(function(){
                                        $('#errorMsg').hide(function(){
                                            $('#errorMsg').addClass('hide');
                                            $('#errorMsg').css("display","block");
                                        });    
                                    },3000);
                            } 
                        });                
                    }
                });
   
            },function(reason){
                console.log(reason);
            }
            
        )
    });//setting logic ends here
    
    $('#sendNoticeForm').on('submit',function(e){//send new notification to user
        e.preventDefault();
        var obj={
            "name":$('#userName').val(),
            "email":$('#userEmail').val(),
            "msg":$('#adminMsg').val()
        }
        $.ajax({
            url:'admin/sendNotice',
            method: "get",
            data:obj,
            dataType:'json',
            success:function(response){
                console.log(response);
                if(response.success){
                    $('#errorMsg>span').html(response.msg);
                    $('#errorMsg').css("color","green");
                    $('#errorMsg').removeClass('fa-times');
                    $('#errorMsg').addClass('fa-check');
                    window.scrollTo(0,0);
                    $('#errorMsg').removeClass('hide');
                    $('#adminMsg').val("");
                    setTimeout(function(){
                        $('#errorMsg').hide(function(){
                            $('#errorMsg').removeClass('fa-check');
                            $('#errorMsg').addClass('fa-times');
                            $('#errorMsg').addClass('hide');
                            $('#errorMsg').css("display","block");
                            $('#errorMsg').css("color","red");
                        });    
                    },3000);
                }else{
                    $('#errorMsg>span').html(response.msg);
                    window.scrollTo(0,0);
                    $('#errorMsg').removeClass('hide');
                    setTimeout(function(){
                        $('#errorMsg').hide(function(){
                            $('#errorMsg').addClass('hide');
                            $('#errorMsg').css("display","block");
                        });    
                    },3000);
                }       
            },
            error:function(response){
                $('#errorMsg>span').html("check your request parameters");
                window.scrollTo(0,0);
                $('#errorMsg').removeClass('hide');
                setTimeout(function(){
                    $('#errorMsg').hide(function(){
                        $('#errorMsg').addClass('hide');
                        $('#errorMsg').css("display","block");
                    });    
                },3000);    
            } 
        });     
    });
  
    $('#userData').on('click',function(){//calling userData function for displaying users data when user click on menu
        changeActiveMenuItem($('#userData'));
        userData();
    });  
    
    
    $('#userNotice').on('click',function(){//calling userNotice function for displaying admin notices when user clicks on menu
       changeActiveMenuItem($('#userNotice'));
       userNotice(); 
    });
    
    
     $('#aboutUs').on('click',function(){//changing admin panel password
        changeActiveMenuItem($('#aboutUs'));
        var loadTemplatePromise=new Promise(function(resolve,reject){
            templateLoader("about",resolve);
        });
     });
    
    
    function userNotice(){// getting admin notices to different users
        $.ajax({
            method:'get',
            url:'/admin/notices',
            dataType:'json',
            success:function(notices){
                $("#right").jsGrid({
                    width: "53%",
                    paging: true,
                    sorting: true,
                    autoload: true,
                    pageSize: 15,
                    editing:true,
                    pageButtonCount: 5,
                    data:notices,
                    controller:{
                            updateItem: function(item) {
                                $('#modalBtn').click();
                                $('#userName').val(item.user_name);
                                $('#userEmail').val(item.user_email);
                            }
                    },
                    fields: [
                            { name: "user_name", title: "User Name", type: "number", width: 255, filtering: false,editing:false },
                            { name: "user_email", title: "User Email", type: "text", width: 255,editing:false },
                            { name: "admin_msg", title: "Message",type:"text", width: 255,editing:false},
                            { name: "created_at", title: "Message Date",type:"text",width:255,editing:false},
                            { type: "control",deleteButton: false  }
                        ]
                    
                });
            }        
        });
    }
    
    function userData(){//getting user data and displaying it in table
        var users;
        $.ajax({
            method:'get',
            url:'/admin/users',
            dataType:'json',
            success:function(users){
                $("#right").jsGrid({
                    width: "53%",
                    paging: true,
                    sorting: true,
                    autoload: true,
                    editing: true,
                    pageSize: 15,
                    pageButtonCount: 5,
                    data:users,
                    controller:{
                            updateItem: function(item) {
                                $('#modalBtn').click();
                                $('#userName').val(item.name);
                                $('#userEmail').val(item.email);
                            }
                    },
                    fields: [
                        { name: "email", title: "Email", type: "text", width: 255, filtering: false,editing:false },
                        { name: "name", title: "Name", type: "text", width: 200,editing:false },
                        { name: "gender", title: "Gender", type: "text", width: 200 ,editing:false},
                        { name: "age", title: "Age", type: "number", width: 30,editing:false },
                        { name: "created_at", title: "Joining Date", width: 200,editing:false},
                        { title: "Send Notice",type: "control",deleteButton: false }
                    ]
                });        
            }
        });
        
    }
    
    
    $(window).on('load',userData());//calling for default userData show
    
    
    
    
})();
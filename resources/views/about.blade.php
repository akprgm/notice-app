<div id="author" class="col-lg-2 col-md-2 col-sm-2 col-xs-12 ">
    <figure><img src="{{ URL::asset('/img/me.jpg') }}"><figcaption>Anil kumar</figure>
    <p>Developer, Gamer and Lover</p>
    <ul class="social">
        <li> <a href="https://www.facebook.com/profile.php?id=100003099420671" title="Friend on Facebook"> <i class=" fa fa-facebook">   </i> </a> </li>
        <li> <a href="mailto:modishcoder@gmail.com" title="Email modishcoder@gmail.com"> <i class="fa fa-envelope">   </i> </a> </li>
        <li> <a href="https://bitbucket.org/modishcoder/easychat" title="Fork me on BitBucket"> <i class="fa fa-bitbucket">   </i> </a> </li>
    </ul>
    <button id="hireMe" class="btn"><a href="https://www.linkedin.com/in/anil-kumar-54a1717b">hire me</a></button>
</div>
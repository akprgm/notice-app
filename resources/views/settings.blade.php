<div id="settingDiv" class="col-md-6 col-lg-6 col-sm-10 col-md-push-3 col-lg-push-3">
    <form id="changePassword" class="form-horizontal">
        <h2>Change Password</h2>
        <div class="form-group">
            <div class="col-sm-10 col-sm-push-2">
                 <aside id="errorMsg" class="hide fa fa-times">&nbsp;&nbsp;<span ></span></aside>
            </div>
        </div>
        <div class="form-group">
            <label for="currentPassword" class="col-sm-2 control-label">Current Password</label>
            <div class="col-sm-10">
            <input type="password" required class="form-control" id="currentPassword" placeholder="Current Password">
            </div>
        </div>
        <div class="form-group">
            <label for="newPassword" class="col-sm-2 control-label">New Password:</label>
            <div class="col-sm-10">
            <input type="password" required class="form-control" id="newPassword" placeholder="New Password">
            </div>
        </div>
        <div class="form-group">
            <label for="confirmPassword" class="col-sm-2 control-label">Confirm Password:</label>
            <div class="col-sm-10">
            <input type="password" required class="form-control" id="confirmPassword" placeholder="Confirm New Password">
            </div>
        </div>
        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button id="login" type="submit" class="btn btn-default btn-success">Change Password</button>
            </div>
        </div>
    </form>
</div>
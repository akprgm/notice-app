<!doctype html>
<html>
    <head>
        <title>Precily</title>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-1.12.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('/bootstrap/js/bootstrap.min.js') }}"></script>
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('/bootstrap/css/bootstrap.min.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('/bootstrap/css/bootstrap-theme.min.css') }}"> 
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('/css/login.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('/font-awesome/css/font-awesome.min.css')}}">
    </head>
<body>
     <header>
        <nav class="navbar navbar-fixed-top">
            <div class="container">
               <h3>NoticeApp</h3>
            </div>
         </nav>
    </header>
    <div id="loginDiv" class="col-lg-4 col-md-4 col-sm-6 col-xs-12 col-lg-push-4 col-md-push-4 col-sm-push-3">
        <form id="login" class="form-horizontal">
            <h2>Login</h2>
            <div class="form-group">
                <div class="col-sm-10 col-sm-push-2">
                    <aside id="errorMsg" class="hide fa fa-times">&nbsp;&nbsp;<span ></span></aside>
                </div>
            </div>
            <div class="form-group">
                <label for="adminEmail" class="col-sm-2 control-label">Email:</label>
                <div class="col-sm-10">
                    <input type="email" required class="form-control" id="adminEmail" placeholder="Email">
                </div>
            </div>
            <div class="form-group">
                <label for="userPwd" class="col-sm-2 control-label">Password:</label>
                <div class="col-sm-10">
                <input type="password" required class="form-control" id="adminPassword" placeholder="Password">
                </div>
            </div>
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button id="loginBtn" type="submit" class="btn btn-default btn-success">Login</button>
                </div>
            </div>
        </form>
        
    </div>
</body>
<script type="text/javascript" src="{{ URL::asset('/js/login.js') }}"></script>
</html>
<!doctype html>
<html>
    <head>
        <title>Precily</title>
        <script type="text/javascript" src="{{ URL::asset('/js/jquery-1.12.4.min.js') }}"></script>
        <script type="text/javascript" src="{{ URL::asset('/bootstrap/js/bootstrap.min.js') }}"></script>
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('/bootstrap/css/bootstrap.min.css') }}">
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('/bootstrap/css/bootstrap-theme.min.css') }}"> 
        <link type="text/css" rel="stylesheet" href="{{ URL::asset('/css/dashboard.css') }}">
        <link rel="stylesheet" href="{{ URL::asset('/font-awesome/css/font-awesome.min.css')}}">
        <script type="text/javascript" src="{{ URL::asset('/jsGrid/jsgrid.js') }}"></script>
        <link rel="stylesheet" href="{{ URL::asset('/jsGrid/jsgrid.min.css')}}">
        <link rel="stylesheet" href="{{ URL::asset('/jsGrid/jsgrid-theme.min.css')}}">
    </head>
<body>
    
    <div id="dashboard" class="row">
        <div id="left" class="col-md-2 col-lg-2">
            <nav id="menu" class="navbar navbar-default" role="navigation">
                <!-- Brand and toggle get grouped for better mobile display -->
                <div class="navbar-header">
                    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a class="navbar-brand" href="">NoticeApp</a>
                </div>

                <!-- Collect the nav links, forms, and other content for toggling -->
                <div class="collapse navbar-collapse navbar-ex1-collapse">
                    <ul class="nav navbar-nav">
                        <li><a id="userData" class="active"><span class="fa fa-user"></span>  Users Data</a></li>
                        <li><a id="userNotice" ><span class="fa fa-bell"></span>  Notices</a></li>
                        <li><a id="setting" ><span class="fa fa-cog"></span>  Setting</a></li>
                        <li><a id="aboutUs" ><span class="fa fa-university"></span> About Us</a></li>
                        <li><a id="logout" href="admin/logout"><span class="fa fa-sign-out"></span> Log Out</a></li>
                    </ul>
                </div>
            </nav>
        </div>
        <div id="right" class="col-lg-10 col-md-10 col-lg-push-2 col-md-push-2">    
        </div>
        
        <!-- Button trigger modal -->
        <button id="modalBtn" type="button" class="btn btn-primary btn-lg hide" data-toggle="modal" data-target="#myModal">
        Launch demo modal
        </button>

        <!-- Modal -->
        <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title" id="myModalLabel">Send Notice</h4>
                    </div>
                    <div class="modal-body">
                        <form id="sendNoticeForm" class="form-horizontal">
                            <div class="form-group">
                                <div class="">
                                    <aside id="errorMsg" class="hide fa fa-times">&nbsp;&nbsp;<span ></span></aside>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userName" class="col-sm-3 control-label">User Name:</label>
                                <div class="col-sm-9 input-group">
                                    <input id="userName" name="user_name" type="text" required class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="userEmail" class="col-sm-3 control-label">User Email:</label>
                                <div class="col-sm-9 input-group">
                                    <input id="userEmail" name="user_email" type="text" required class="form-control"/>
                                </div>
                            </div>
                            <div class="form-group">
                                <label for="adminMsg" class="col-sm-3 control-label">Message:</label>
                                <div class="col-sm-9 input-group">
                                    <textarea id="adminMsg" name="admin_msg" type="text" required rows="2" maxlength="255" class="form-control"></textarea>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button id="sendNoticeBtn" type="submit" class="btn btn-default btn-success">Send Notice</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> 
    
</body>
<script type="text/javascript" src="{{ URL::asset('/js/dashboard.js') }}"></script>
</html>